/*
 * Copyright (c) 1997, 2006, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.formats.html.markup;

import java.io.*;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;

import ragdoll.html.*;

/**
 * Class for the Html format code generation.
 * Initilizes PrintWriter with FileWriter, to enable print
 * related methods to generate the code to the named File through FileWriter.
 *
 * @since 1.2
 * @author Atul M Dambalkar
 * @author Jesper Öqvist
 */
public class HtmlWriter {
    
    public void print(char c) {
        container.addText(""+c);
    }

    public void print(String s) {
        container.addText(s);
    }

    public void println() {
        container.addText("\n");
    }

    public void println(String x) {
        container.addText(x+"\n");
    }

    /**
     * Write the html document.
     */
    public void close() {
        try {
            root.writeHtml(writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            
        }
    }

    protected Container root = new HtmlRoot();
    protected Container container = root;

    /**
     * Name of the file, to which this writer is writing to.
     */
    protected final String htmlFilename;

    /**
     * URL file separator string("/").
     */
    public static final String fileseparator =
         DirectoryManager.URL_FILE_SEPERATOR;

    /**
     * The configuration
     */
    protected Configuration configuration;
    private Writer writer;

    /**
     * Constructor.
     *
     * @param path The directory path to be created for this file
     *             or null if none to be created.
     * @param filename File Name to which the PrintWriter will
     *                 do the Output.
     * @param docencoding Encoding to be used for this file.
     * @exception IOException Exception raised by the FileWriter is passed on
     * to next level.
     * @exception UnSupportedEncodingException Exception raised by the
     * OutputStreamWriter is passed on to next level.
     */
    public HtmlWriter(Configuration configuration,
                      String path, String filename, String docencoding)
                      throws IOException, UnsupportedEncodingException {
        writer = Util.genWriter(configuration, path, filename, docencoding);
        this.configuration = configuration;
        htmlFilename = filename;
    }
    
    public void div() {
        Container c = new Div();
        container.add(c);
        container = c;
    }
    
    public void span() {
        Container c = new Span();
        container.add(c);
        container = c;
    }
    
    public void addAttribute(String name, String value) {
        container.addAttribute(name, value);
    }
    
    public void attr(String name, String value) {
        addAttribute(name, value);
    }
    
    /**
     * Print &lt;HTML&gt; tag. Add a newline character at the end.
     */
    public void html() {
        Html html = new Html();
        container.add(html);
        container = html;
    }

    /**
     * Print &lt;/HTML&gt; tag. Add a newline character at the end.
     */
    public void htmlEnd() {
        closeTag();
    }
    
    public void closeTag() {
        container = container.getContainer();
    }

    /**
     * Print the Javascript &lt;SCRIPT&gt; start tag with its type
     * attribute.
     */
    public void script() {
        println("<SCRIPT type=\"text/javascript\">");
    }

    /**
     * Print the Javascript &lt;/SCRIPT&gt; end tag.
     */
    public void scriptEnd() {
        println("</SCRIPT>");
    }

    /**
     * Print the Javascript &lt;NOSCRIPT&gt; start tag.
     */
    public void noScript() {
        println("<NOSCRIPT>");
    }

    /**
     * Print the Javascript &lt;/NOSCRIPT&gt; end tag.
     */
    public void noScriptEnd() {
        println("</NOSCRIPT>");
    }

    public void body() {
        Body c = new Body();
        container.add(c);
        container = c;
    }

    /**
     * Print &lt;/BODY&gt; tag. Add a newline character at the end.
     */
    public void bodyEnd() {
        closeTag();
    }

    /**
     * Print &lt;TITLE&gt; tag. Add a newline character at the end.
     */
    public void title() {
        println("<TITLE>");
    }

    /**
     * Print &lt;TITLE&gt; tag. Add a newline character at the end.
     *
     * @param winTitle The title of this document.
     */
    public void title(String winTitle) {
        container.add(new Title(new ragdoll.html.List<Attribute>(), winTitle));
    }


    /**
     * Print &lt;/TITLE&gt; tag. Add a newline character at the end.
     */
    public void titleEnd() {
        println("</TITLE>");
    }

    /**
     * Print &lt;UL&gt; tag. Add a newline character at the end.
     */
    public void ul() {
        println("<UL>");
    }

    /**
     * Print &lt;/UL&gt; tag. Add a newline character at the end.
     */
    public void ulEnd() {
        println("</UL>");
    }

    /**
     * Print &lt;LI&gt; tag.
     */
    public void li() {
        ListItem li = new ListItem();
        container.add(li);
        container = li;
    }

    /**
     * Print &lt;LI TYPE="type"&gt; tag.
     *
     * @param type Type string.
     */
    public void li(String type) {
        print("<LI TYPE=\"" + type + "\">");
    }

    /**
     * Print &lt;H1&gt; tag. Add a newline character at the end.
     */
    public void h1() {
        Heading h1 = new H1();
        container.add(h1);
        container = h1;
    }

    /**
     * Print &lt;/H1&gt; tag. Add a newline character at the end.
     */
    public void h1End() {
        closeTag();
    }

    /**
     * Print text with &lt;H1&gt; tag. Also adds &lt;/H1&gt; tag. Add a newline character
     * at the end of the text.
     *
     * @param text Text to be printed with &lt;H1&gt; format.
     */
    public void h1(String text) {
        h1();
        println(text);
        h1End();
    }

    /**
     * Print &lt;H2&gt; tag. Add a newline character at the end.
     */
    public void h2() {
        Heading h2 = new H2();
        container.add(h2);
        container = h2;
    }

    /**
     * Print text with &lt;H2&gt; tag. Also adds &lt;/H2&gt; tag. Add a newline character
     *  at the end of the text.
     *
     * @param text Text to be printed with &lt;H2&gt; format.
     */
    public void h2(String text) {
        h2();
        print(text);
        h2End();
    }

    /**
     * Print &lt;/H2&gt; tag. Add a newline character at the end.
     */
    public void h2End() {
        closeTag();
    }

    /**
     * Print &lt;H3&gt; tag. Add a newline character at the end.
     */
    public void h3() {
        Heading h3 = new H3();
        container.add(h3);
        container = h3;
    }

    /**
     * Print text with &lt;H3&gt; tag. Also adds &lt;/H3&gt; tag. Add a newline character
     *  at the end of the text.
     *
     * @param text Text to be printed with &lt;H3&gt; format.
     */
    public void h3(String text) {
        h3();
        println(text);
        h3End();
    }

    /**
     * Print &lt;/H3&gt; tag. Add a newline character at the end.
     */
    public void h3End() {
        closeTag();
    }

    /**
     * Print &lt;H4&gt; tag. Add a newline character at the end.
     */
    public void h4() {
        Heading h4 = new H4();
        container.add(h4);
        container = h4;
    }

    /**
     * Print &lt;/H4&gt; tag. Add a newline character at the end.
     */
    public void h4End() {
        closeTag();
    }

    /**
     * Print text with &lt;H4&gt; tag. Also adds &lt;/H4&gt; tag. Add a newline character
     * at the end of the text.
     *
     * @param text Text to be printed with &lt;H4&gt; format.
     */
    public void h4(String text) {
        h4();
        println(text);
        h4End();
    }

    /**
     * Print &lt;H5&gt; tag. Add a newline character at the end.
     */
    public void h5() {
        Heading h5 = new H5();
        container.add(h5);
        container = h5;
    }

    /**
     * Print &lt;/H5&gt; tag. Add a newline character at the end.
     */
    public void h5End() {
        closeTag();
    }

    /**
     * Print HTML &lt;IMG SRC="imggif" WIDTH="width" HEIGHT="height" ALT="imgname&gt;
     * tag. It prepends the "images" directory name to the "imggif". This
     * method is used for oneone format generation. Add a newline character
     * at the end.
     *
     * @param imggif   Image GIF file.
     * @param imgname  Image name.
     * @param width    Width of the image.
     * @param height   Height of the image.
     */
    public void img(String imggif, String imgname, int width, int height) {
        println("<IMG SRC=\"images/" + imggif + ".gif\""
              + " WIDTH=\"" + width + "\" HEIGHT=\"" + height
              + "\" ALT=\"" + imgname + "\">");
    }

    /**
     * Print &lt;MENU&gt; tag. Add a newline character at the end.
     */
    public void menu() {
        println("<MENU>");
    }

    /**
     * Print &lt;/MENU&gt; tag. Add a newline character at the end.
     */
    public void menuEnd() {
        println("</MENU>");
    }

    /**
     * Print &lt;PRE&gt; tag. Add a newline character at the end.
     */
    public void pre() {
        Container c = new Pre();
        container.add(c);
        container = c;
    }

    /**
     * Print &lt;PRE&gt; tag without adding new line character at th eend.
     */
    public void preNoNewLine() {
        Container c = new Pre();
        container.add(c);
        container = c;
    }

    /**
     * Print &lt;/PRE&gt; tag. Add a newline character at the end.
     */
    public void preEnd() {
        closeTag();
    }

    /**
     * Get the "&lt;B&gt;" string.
     *
     * @return String Return String "&lt;B&gt;";
     */
    public String getBold() {
        return "<b>";
    }

    /**
     * Get the "&lt;/B&gt;" string.
     *
     * @return String Return String "&lt;/B&gt;";
     */
    public String getBoldEnd() {
        return "</b>";
    }

    /**
     * Print &lt;B&gt; tag.
     */
    public void bold() {
        Bold bold = new Bold();
        container.add(bold);
        container = bold;
    }

    /**
     * Print &lt;/B&gt; tag.
     */
    public void boldEnd() {
        closeTag();
    }

    /**
     * Print text passed, in bold format using &lt;B&gt; and &lt;/B&gt; tags.
     *
     * @param text String to be printed in between &lt;B&gt; and &lt;/B&gt; tags.
     */
    public void bold(String text) {
        Bold bold = new Bold();
        bold.addText(text);
        container.add(bold);
    }

    /**
     * Print text passed, in Italics using &lt;I&gt; and &lt;/I&gt; tags.
     *
     * @param text String to be printed in between &lt;I&gt; and &lt;/I&gt; tags.
     */
    public void italics(String text) {
        Emphasis em = new Emphasis();
        em.addText(text);
        container.add(em);
    }

    /**
     * Return, text passed, with Italics &lt;I&gt; and &lt;/I&gt; tags, surrounding it.
     * So if the text passed is "Hi", then string returned will be "&lt;I&gt;Hi&lt;/I&gt;".
     *
     * @param text String to be printed in between &lt;I&gt; and &lt;/I&gt; tags.
     */
    public String italicsText(String text) {
        return "<I>" + text + "</I>";
    }

    public String codeText(String text) {
        return "<CODE>" + text + "</CODE>";
    }

    /**
     * Print "&#38;nbsp;", non-breaking space.
     */
    public void space() {
        print("&nbsp;");
    }

    /**
     * Print &lt;DL&gt; tag. Add a newline character at the end.
     */
    public void dl() {
        println("<DL>");
    }

    /**
     * Print &lt;/DL&gt; tag. Add a newline character at the end.
     */
    public void dlEnd() {
        println("</DL>");
    }

    /**
     * Print &lt;DT&gt; tag.
     */
    public void dt() {
        print("<DT>");
    }

    /**
     * Print &lt;DT&gt; tag.
     */
    public void dd() {
        print("<DD>");
    }

    /**
     * Print &lt;/DD&gt; tag. Add a newline character at the end.
     */
    public void ddEnd() {
        println("</DD>");
    }

    /**
     * Print &lt;SUP&gt; tag. Add a newline character at the end.
     */
    public void sup() {
        Sup sup = new Sup();
        container.add(sup);
        container = sup;
    }

    /**
     * Print &lt;/SUP&gt; tag. Add a newline character at the end.
     */
    public void supEnd() {
        closeTag();
    }

    /**
     * Print &lt;FONT SIZE="size"&gt; tag. Add a newline character at the end.
     *
     * @param size String size.
     */
    @Deprecated
    public void font(String size) {
        println("<FONT SIZE=\"" + size + "\">");
    }

    /**
     * Print &lt;FONT SIZE="size"&gt; tag.
     *
     * @param size String size.
     */
    @Deprecated
    public void fontNoNewLine(String size) {
        print("<FONT SIZE=\"" + size + "\">");
    }

    /**
     * Print &lt;FONT CLASS="stylename"&gt; tag. Add a newline character at the end.
     *
     * @param stylename String stylename.
     */
    @Deprecated
    public void fontStyle(String stylename) {
        print("<FONT CLASS=\"" + stylename + "\">");
    }

    /**
     * Print &lt;FONT SIZE="size" CLASS="stylename"&gt; tag. Add a newline character
     * at the end.
     *
     * @param size String size.
     * @param stylename String stylename.
     */
    @Deprecated
    public void fontSizeStyle(String size, String stylename) {
        println("<FONT size=\"" + size + "\" CLASS=\"" + stylename + "\">");
    }

    /**
     * Print &lt;/FONT&gt; tag.
     */
    @Deprecated
    public void fontEnd() {
        print("</FONT>");
    }

    /**
     * Get the "&lt;FONT COLOR="color"&gt;" string.
     *
     * @param color String color.
     * @return String Return String "&lt;FONT COLOR="color"&gt;".
     */
    public String getFontColor(String color) {
        return "<FONT COLOR=\"" + color + "\">";
    }

    /**
     * Get the "&lt;/FONT&gt;" string.
     *
     * @return String Return String "&lt;/FONT&gt;";
     */
    public String getFontEnd() {
        return "</FONT>";
    }

    /**
     * Print &lt;CENTER&gt; tag. Add a newline character at the end.
     */
    @Deprecated
    public void center() {
        println("<CENTER>");
    }

    /**
     * Print &lt;/CENTER&gt; tag. Add a newline character at the end.
     */
    @Deprecated
    public void centerEnd() {
        println("</CENTER>");
    }

    /**
     * Print anchor &lt;A NAME="name"&gt; tag.
     *
     * @param name Name String.
     */
    public void aName(String name) {
        Anchor a = new Anchor();
        a.setName(name);
        container.add(a);
        container = a;
    }

    /**
     * Print &lt;/A&gt; tag.
     */
    public void aEnd() {
        closeTag();
    }

    /**
     * Print &lt;I&gt; tag.
     */
    public void italic() {
        Emphasis em = new Emphasis();
        container.add(em);
        container = em;
    }

    /**
     * Print &lt;/I&gt; tag.
     */
    public void italicEnd() {
        closeTag();
    }

    /**
     * Print contents within anchor &lt;A NAME="name"&gt; tags.
     *
     * @param name String name.
     * @param content String contents.
     */
    public void anchor(String name, String content) {
        aName(name);
        print(content);
        aEnd();
    }

    /**
     * Print anchor &lt;A NAME="name"&gt; and &lt;/A&gt;tags. Print comment string
     * "&lt;!-- --&gt;" within those tags.
     *
     * @param name String name.
     */
    public void anchor(String name) {
        anchor(name, "");
    }

    /**
     * Print newline and then print &lt;P&gt; tag. Add a newline character at the
     * end.
     */
    public void p() {
        Paragraph p = new Paragraph();
        container.add(p);
        container = p;
    }

    /**
     * Print newline and then print &lt;/P&gt; tag. Add a newline character at the
     * end.
     */
    public void pEnd() {
        closeTag();
    }

    /**
     * Print newline and then print &lt;BR&gt; tag. Add a newline character at the
     * end.
     */
    public void br() {
        println();
        println("<br>");
    }

    /**
     * Print &lt;ADDRESS&gt; tag. Add a newline character at the end.
     */
    public void address() {
        println("<address>");
    }

    /**
     * Print &lt;/ADDRESS&gt; tag. Add a newline character at the end.
     */
    public void addressEnd() {
        println("</address>");
    }

    /**
     * Print &lt;HEAD&gt; tag. Add a newline character at the end.
     */
    public void head() {
        Head head = new Head();
        container.add(head);
        container = head;
    }

    /**
     * Print &lt;/HEAD&gt; tag. Add a newline character at the end.
     */
    public void headEnd() {
        closeTag();
    }

    /**
     * Print &lt;CODE&gt; tag.
     */
    public void code() {
        Code code = new Code();
        container.add(code);
        container = code;
    }

    /**
     * Print &lt;/CODE&gt; tag.
     */
    public void codeEnd() {
        closeTag();
    }

    /**
     * Print &lt;EM&gt; tag. Add a newline character at the end.
     */
    public void em() {
        Emphasis em = new Emphasis();
        container.add(em);
        container = em;
    }

    /**
     * Print &lt;/EM&gt; tag. Add a newline character at the end.
     */
    public void emEnd() {
        closeTag();
    }

    /**
     * Print HTML &lt;TABLE BORDER="border" WIDTH="width"
     * CELLPADDING="cellpadding" CELLSPACING="cellspacing"&gt; tag.
     *
     * @param border       Border size.
     * @param width        Width of the table.
     * @param cellpadding  Cellpadding for the table cells.
     * @param cellspacing  Cellspacing for the table cells.
     */
    public void table(int border, String width, int cellpadding,
                      int cellspacing) {
        println(DocletConstants.NL +
                "<TABLE BORDER=\"" + border +
                "\" WIDTH=\"" + width +
                "\" CELLPADDING=\"" + cellpadding +
                "\" CELLSPACING=\"" + cellspacing +
                "\" SUMMARY=\"\">");
    }

    /**
     * Print HTML &lt;TABLE BORDER="border" CELLPADDING="cellpadding"
     * CELLSPACING="cellspacing"&gt; tag.
     *
     * @param border       Border size.
     * @param cellpadding  Cellpadding for the table cells.
     * @param cellspacing  Cellspacing for the table cells.
     */
    public void table(int border, int cellpadding, int cellspacing) {
        println(DocletConstants.NL +
                "<TABLE BORDER=\"" + border +
                "\" CELLPADDING=\"" + cellpadding +
                "\" CELLSPACING=\"" + cellspacing +
                "\" SUMMARY=\"\">");
    }

    /**
     * Print HTML &lt;TABLE BORDER="border" WIDTH="width"&gt;
     *
     * @param border       Border size.
     * @param width        Width of the table.
     */
    public void table(int border, String width) {
        println(DocletConstants.NL +
                "<TABLE BORDER=\"" + border +
                "\" WIDTH=\"" + width +
                "\" SUMMARY=\"\">");
    }

    /**
     * Print the HTML table tag with border size 0 and width 100%.
     */
    public void table() {
        table(0, "100%");
    }

    /**
     * Print &lt;/TABLE&gt; tag. Add a newline character at the end.
     */
    public void tableEnd() {
        println("</TABLE>");
    }

    /**
     * Print &lt;TR&gt; tag. Add a newline character at the end.
     */
    public void tr() {
        println("<TR>");
    }

    /**
     * Print &lt;/TR&gt; tag. Add a newline character at the end.
     */
    public void trEnd() {
        println("</TR>");
    }

    /**
     * Print &lt;TD&gt; tag.
     */
    public void td() {
        print("<TD>");
    }

    /**
     * Print &lt;TD NOWRAP&gt; tag.
     */
    public void tdNowrap() {
        print("<TD NOWRAP>");
    }

    /**
     * Print &lt;TD WIDTH="width"&gt; tag.
     *
     * @param width String width.
     */
    public void tdWidth(String width) {
        print("<TD WIDTH=\"" + width + "\">");
    }

    /**
     * Print &lt;/TD&gt; tag. Add a newline character at the end.
     */
    public void tdEnd() {
        println("</TD>");
    }

    /**
     * Print &lt;LINK str&gt; tag.
     *
     * @param str String.
     */
    public void link(String str) {
        println("<LINK " + str + ">");
    }

    /**
     * Print "&lt;!-- " comment start string.
     */
    public void commentStart() {
        Comment comment = new Comment();
        container.add(comment);
        container = comment;
    }

    /**
     * Print "--&gt;" comment end string. Add a newline character at the end.
     */
    public void commentEnd() {
        closeTag();
    }

    /**
     * Print &lt;TR BGCOLOR="color" CLASS="stylename"&gt; tag. Adds a newline character
     * at the end.
     *
     * @param color String color.
     * @param stylename String stylename.
     */
    public void trBgcolorStyle(String color, String stylename) {
        println("<TR BGCOLOR=\"" + color + "\" CLASS=\"" + stylename + "\">");
    }

    /**
     * Print &lt;TR BGCOLOR="color"&gt; tag. Adds a newline character at the end.
     *
     * @param color String color.
     */
    public void trBgcolor(String color) {
        println("<TR BGCOLOR=\"" + color + "\">");
    }

    /**
     * Print &lt;TR ALIGN="align" VALIGN="valign"&gt; tag. Adds a newline character
     * at the end.
     *
     * @param align String align.
     * @param valign String valign.
     */
    public void trAlignVAlign(String align, String valign) {
        println("<TR ALIGN=\"" + align + "\" VALIGN=\"" + valign + "\">");
    }

    /**
     * Print &lt;TH ALIGN="align"&gt; tag.
     *
     * @param align the align attribute.
     */
    public void thAlign(String align) {
        print("<TH ALIGN=\"" + align + "\">");
    }

    /**
     * Print &lt;TH align="align" COLSPAN=i&gt; tag.
     *
     * @param align the align attribute.
     * @param i integer.
     */
    public void thAlignColspan(String align, int i) {
        print("<TH ALIGN=\"" + align + "\" COLSPAN=\"" + i + "\">");
    }

    /**
     * Print &lt;TH align="align" NOWRAP&gt; tag.
     *
     * @param align the align attribute.
     */
    public void thAlignNowrap(String align) {
        print("<TH ALIGN=\"" + align + "\" NOWRAP>");
    }

    /**
     * Print &lt;/TH&gt; tag. Add a newline character at the end.
     */
    public void thEnd() {
        println("</TH>");
    }

    /**
     * Print &lt;TD COLSPAN=i&gt; tag.
     *
     * @param i integer.
     */
    public void tdColspan(int i) {
        print("<TD COLSPAN=" + i + ">");
    }

    /**
     * Print &lt;TD BGCOLOR="color" CLASS="stylename"&gt; tag.
     *
     * @param color String color.
     * @param stylename String stylename.
     */
    public void tdBgcolorStyle(String color, String stylename) {
        print("<TD BGCOLOR=\"" + color + "\" CLASS=\"" + stylename + "\">");
    }

    /**
     * Print &lt;TD COLSPAN=i BGCOLOR="color" CLASS="stylename"&gt; tag.
     *
     * @param i integer.
     * @param color String color.
     * @param stylename String stylename.
     */
    public void tdColspanBgcolorStyle(int i, String color, String stylename) {
        print("<TD COLSPAN=" + i + " BGCOLOR=\"" + color + "\" CLASS=\"" +
              stylename + "\">");
    }

    /**
     * Print &lt;TD ALIGN="align"&gt; tag. Adds a newline character
     * at the end.
     *
     * @param align String align.
     */
    public void tdAlign(String align) {
        print("<TD ALIGN=\"" + align + "\">");
    }

    /**
     * Print &lt;TD ALIGN="align" CLASS="stylename"&gt; tag.
     *
     * @param align        String align.
     * @param stylename    String stylename.
     */
    public void tdVAlignClass(String align, String stylename) {
        print("<TD VALIGN=\"" + align + "\" CLASS=\"" + stylename + "\">");
    }

    /**
     * Print &lt;TD VALIGN="valign"&gt; tag.
     *
     * @param valign String valign.
     */
    public void tdVAlign(String valign) {
        print("<TD VALIGN=\"" + valign + "\">");
    }

    /**
     * Print &lt;TD ALIGN="align" VALIGN="valign"&gt; tag.
     *
     * @param align   String align.
     * @param valign  String valign.
     */
    public void tdAlignVAlign(String align, String valign) {
        print("<TD ALIGN=\"" + align + "\" VALIGN=\"" + valign + "\">");
    }

    /**
     * Print &lt;TD ALIGN="align" ROWSPAN=rowspan&gt; tag.
     *
     * @param align    String align.
     * @param rowspan  integer rowspan.
     */
    public void tdAlignRowspan(String align, int rowspan) {
        print("<TD ALIGN=\"" + align + "\" ROWSPAN=" + rowspan + ">");
    }

    /**
     * Print &lt;TD ALIGN="align" VALIGN="valign" ROWSPAN=rowspan&gt; tag.
     *
     * @param align    String align.
     * @param valign  String valign.
     * @param rowspan  integer rowspan.
     */
    public void tdAlignVAlignRowspan(String align, String valign,
                                     int rowspan) {
        print("<TD ALIGN=\"" + align + "\" VALIGN=\"" + valign
                + "\" ROWSPAN=" + rowspan + ">");
    }

    /**
     * Print &lt;BLOCKQUOTE&gt; tag. Add a newline character at the end.
     */
    public void blockquote() {
        println("<BLOCKQUOTE>");
    }

    /**
     * Print &lt;/BLOCKQUOTE&gt; tag. Add a newline character at the end.
     */
    public void blockquoteEnd() {
        println("</BLOCKQUOTE>");
    }

    /**
     * Get the "&lt;CODE&gt;" string.
     *
     * @return String Return String "&lt;CODE>";
     */
    public String getCode() {
        return "<CODE>";
    }

    /**
     * Get the "&lt;/CODE&gt;" string.
     *
     * @return String Return String "&lt;/CODE&gt;";
     */
    public String getCodeEnd() {
        return "</CODE>";
    }

    /**
     * Print &lt;NOFRAMES&gt; tag. Add a newline character at the end.
     */
    public void noFrames() {
        println("<NOFRAMES>");
    }

    /**
     * Print &lt;/NOFRAMES&gt; tag. Add a newline character at the end.
     */
    public void noFramesEnd() {
        println("</NOFRAMES>");
    }
}
