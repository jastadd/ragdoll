/*
 * Copyright (c) 2003, 2004, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.formats.html;

import com.sun.javadoc.*;

import java.io.*;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.taglets.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;

/**
 * Writes enum constant documentation in HTML format.
 *
 * @author Jamie Ho
 */
public class EnumConstantWriterImpl extends AbstractMemberWriter
    implements EnumConstantWriter, MemberSummaryWriter {

    public EnumConstantWriterImpl(SubWriterHolderWriter writer,
        ClassDoc classdoc) {
        super(writer, classdoc);
    }

    public EnumConstantWriterImpl(SubWriterHolderWriter writer) {
        super(writer);
    }

    /**
     * Write the inherited enum constant summary header for the given class.
     *
     * @param classDoc the class the summary belongs to.
     */
    public void writeInheritedMemberSummaryHeader(ClassDoc classDoc) {
        writer.printInheritedSummaryHeader(this, classDoc);
    }

    /**
     * {@inheritDoc}
     */
    public void writeInheritedMemberSummary(ClassDoc classDoc,
        ProgramElementDoc enumConstant, boolean isFirst, boolean isLast) {
        if (! isFirst) {
            print(", ");
        }
        writer.printInheritedSummaryMember(this, classDoc, enumConstant);
    }

    /**
     * {@inheritDoc}
     */
    public void writeHeader(ClassDoc classDoc, String header) {
        writer.println();
        writer.println("<!-- ============ ENUM CONSTANT DETAIL =========== -->");
        writer.println();
        writer.anchor("enum_constant_detail");
        writer.printTableHeadingBackground(header);
        writer.println();
    }

    /**
     * {@inheritDoc}
     */
    public void writeSignature(FieldDoc enumConstant) {
        writer.div();
        writer.addAttribute("class", "member-heading");
        writer.anchor(enumConstant.name());
        writer.pre();
        writer.print("<img width=\"32\" height=\"32\" src=\""+writer.relativePath+"resources/syn.png\">");
        writer.writeAnnotationInfo(enumConstant);
        printModifiers(enumConstant);
        writer.printLink(new LinkInfoImpl(LinkInfoImpl.CONTEXT_MEMBER,
            enumConstant.type()));
        print(' ');
        if (configuration().linksource) {
            writer.printSrcLink(enumConstant, enumConstant.name());
        } else {
            bold(enumConstant.name());
        }
        writer.closeTag();
        writer.closeTag();
    }

    /**
     * {@inheritDoc}
     */
    public void writeDeprecated(FieldDoc enumConstant) {
        print(((TagletOutputImpl)
            (new DeprecatedTaglet()).getTagletOutput(enumConstant,
            writer.getTagletWriterInstance(false))).toString());
    }

    /**
     * {@inheritDoc}
     */
    public void writeComments(FieldDoc enumConstant) {
        if (enumConstant.inlineTags().length > 0) {
            writer.dd();
            writer.printInlineComment(enumConstant);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void writeTags(FieldDoc enumConstant) {
        writer.printTags(enumConstant);
    }

    /**
     * {@inheritDoc}
     */
    public void writeEnumConstantFooter() {
        writer.dlEnd();
    }

    /**
     * {@inheritDoc}
     */
    public void writeFooter(ClassDoc classDoc) {
        //No footer to write for enum constant documentation
    }

    /**
     * {@inheritDoc}
     */
    public void close() throws IOException {
        writer.close();
    }

    public int getMemberKind() {
        return VisibleMemberMap.ENUM_CONSTANTS;
    }

    public String summaryLabel(ClassDoc cd) {
    	return writer.getText("doclet.Enum_Constant_Summary");
    }
    
    public void printSummaryLabel(ClassDoc cd) {
        writer.boldText("doclet.Enum_Constant_Summary");
    }

    public void printSummaryAnchor(ClassDoc cd) {
        writer.anchor("enum_constant_summary");
    }

    public void printInheritedSummaryAnchor(ClassDoc cd) {
    }   // no such

    public void printInheritedSummaryLabel(ClassDoc cd) {
        // no such
    }

    protected void writeSummaryLink(int context, ClassDoc cd, ProgramElementDoc member) {
        writer.bold();
        writer.printDocLink(context, (MemberDoc) member, member.name(), false);
        writer.boldEnd();
    }

    protected void writeInheritedSummaryLink(ClassDoc cd,
            ProgramElementDoc member) {
        writer.printDocLink(LinkInfoImpl.CONTEXT_MEMBER, (MemberDoc)member,
            member.name(), false);
    }

    protected void printSummaryType(ProgramElementDoc member) {
        //Not applicable.
    }

    protected void writeDeprecatedLink(ProgramElementDoc member) {
        writer.printDocLink(LinkInfoImpl.CONTEXT_MEMBER,
            (MemberDoc) member, ((FieldDoc)member).qualifiedName(), false);
    }

    protected void printNavSummaryLink(ClassDoc cd, boolean link) {
        if (link) {
            writer.printHyperLink("", (cd == null)?
                        "enum_constant_summary":
                        "enum_constants_inherited_from_class_" +
                        configuration().getClassName(cd),
                    configuration().getText("doclet.navEnum"));
        } else {
            writer.printText("doclet.navEnum");
        }
    }

    protected void printNavDetailLink(boolean link) {
        if (link) {
            writer.printHyperLink("", "enum_constant_detail",
                configuration().getText("doclet.navEnum"));
        } else {
            writer.printText("doclet.navEnum");
        }
    }

	@Override
	public String summaryId() {
		return "enum_constant_summary";
	}
}
