/*
 * Copyright (c) 1997, 2004, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.formats.html;

import com.sun.javadoc.*;

import java.io.*;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.taglets.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;

/**
 * Writes field documentation in HTML format.
 *
 * @author Robert Field
 * @author Atul M Dambalkar
 * @author Jamie Ho (rewrite)
 */
public class FieldWriterImpl extends AbstractMemberWriter
    implements FieldWriter, MemberSummaryWriter {

    public FieldWriterImpl(SubWriterHolderWriter writer, ClassDoc classdoc) {
        super(writer, classdoc);
    }

    public FieldWriterImpl(SubWriterHolderWriter writer) {
        super(writer);
    }

    /**
     * Write the fields summary footer for the given class.
     *
     * @param classDoc the class the summary belongs to.
     */
    public void writeMemberSummaryFooter(ClassDoc classDoc) {
        writer.tableEnd();
        writer.space();
    }

    /**
     * Write the inherited fields summary header for the given class.
     *
     * @param classDoc the class the summary belongs to.
     */
    public void writeInheritedMemberSummaryHeader(ClassDoc classDoc) {
        writer.printInheritedSummaryHeader(this, classDoc);
    }

    /**
     * {@inheritDoc}
     */
    public void writeInheritedMemberSummary(ClassDoc classDoc,
        ProgramElementDoc field, boolean isFirst, boolean isLast) {
        writer.printInheritedSummaryMember(this, classDoc, field);
    }

    /**
     * Write the header for the field documentation.
     *
     * @param classDoc the class that the fields belong to.
     */
    public void writeHeader(ClassDoc classDoc, String header) {
        writer.anchor("field_detail");
        writer.h2(header);
    }

    /**
     * Write the signature for the given field.
     *
     * @param field the field being documented.
     */
    public void writeSignature(FieldDoc field) {
        writer.div();
        writer.addAttribute("class", "member-heading");
        writer.anchor(field.name());
        writer.pre();
        writer.writeAnnotationInfo(field);
        printModifiers(field);
        writer.printLink(new LinkInfoImpl(LinkInfoImpl.CONTEXT_MEMBER,
            field.type()));
        print(' ');
        if (configuration().linksource) {
            writer.printSrcLink(field, field.name());
        } else {
            bold(field.name());
        }
        writer.closeTag();
        writer.closeTag();
    }

    /**
     * Write the deprecated output for the given field.
     *
     * @param field the field being documented.
     */
    public void writeDeprecated(FieldDoc field) {
        print(((TagletOutputImpl)
            (new DeprecatedTaglet()).getTagletOutput(field,
            writer.getTagletWriterInstance(false))).toString());
    }

    /**
     * Write the comments for the given field.
     *
     * @param field the field being documented.
     */
    public void writeComments(FieldDoc field) {
        ClassDoc holder = field.containingClass();
        if (field.inlineTags().length > 0) {
            if (holder.equals(classdoc) ||
                (! (holder.isPublic() || Util.isLinkable(holder, configuration())))) {
                writer.dd();
                writer.printInlineComment(field);
            } else {
                String classlink = writer.codeText(
                    writer.getDocLink(LinkInfoImpl.CONTEXT_FIELD_DOC_COPY,
                        holder, field,
                        holder.isIncluded() ?
                            holder.typeName() : holder.qualifiedTypeName(),
                        false));
                writer.dd();
                writer.bold(configuration().getText(holder.isClass()?
                   "doclet.Description_From_Class" :
                    "doclet.Description_From_Interface", classlink));
                writer.ddEnd();
                writer.dd();
                writer.printInlineComment(field);
            }
        }
    }

    /**
     * Write the tag output for the given field.
     *
     * @param field the field being documented.
     */
    public void writeTags(FieldDoc field) {
        writer.printTags(field);
    }

    /**
     * Write the field footer.
     */
    public void writeFieldFooter() {
        writer.dlEnd();
    }

    /**
     * Write the footer for the field documentation.
     *
     * @param classDoc the class that the fields belong to.
     */
    public void writeFooter(ClassDoc classDoc) {
        //No footer to write for field documentation
    }

    /**
     * Close the writer.
     */
    public void close() throws IOException {
        writer.close();
    }

    public int getMemberKind() {
        return VisibleMemberMap.FIELDS;
    }

    public String summaryLabel(ClassDoc cd) {
    	return writer.getText("doclet.Field_Summary");
    }
    
    public void printSummaryLabel(ClassDoc cd) {
        writer.boldText("doclet.Field_Summary");
    }

    public void printSummaryAnchor(ClassDoc cd) {
        writer.anchor("field_summary");
    }

    public void printInheritedSummaryAnchor(ClassDoc cd) {
        writer.anchor("fields_inherited_from_class_" + configuration().getClassName(cd));
    }

    public void printInheritedSummaryLabel(ClassDoc cd) {
        String classlink = writer.getPreQualifiedClassLink(
            LinkInfoImpl.CONTEXT_MEMBER, cd, false);
        writer.bold();
        String key = cd.isClass()?
            "doclet.Fields_Inherited_From_Class" :
            "doclet.Fields_Inherited_From_Interface";
        writer.printText(key, classlink);
        writer.boldEnd();
    }

    protected void writeSummaryLink(int context, ClassDoc cd, ProgramElementDoc member) {
        writer.bold();
        writer.printDocLink(context, cd , (MemberDoc) member, member.name(), false);
        writer.boldEnd();
    }

    protected void writeInheritedSummaryLink(ClassDoc cd,
            ProgramElementDoc member) {
        writer.div();
        writer.attr("class", "inherited-member");
        writer.printDocLink(LinkInfoImpl.CONTEXT_MEMBER, cd, (MemberDoc)member,
            member.name(), false);
        writer.closeTag();
    }

    protected void printSummaryType(ProgramElementDoc member) {
        FieldDoc field = (FieldDoc)member;
        printModifierAndType(field, field.type());
    }

    protected void writeDeprecatedLink(ProgramElementDoc member) {
        writer.printDocLink(LinkInfoImpl.CONTEXT_MEMBER,
            (MemberDoc) member, ((FieldDoc)member).qualifiedName(), false);
    }

    protected void printNavSummaryLink(ClassDoc cd, boolean link) {
        if (link) {
            writer.printHyperLink("", (cd == null)?
                        "field_summary":
                        "fields_inherited_from_class_" +
                        configuration().getClassName(cd),
                    configuration().getText("doclet.navField"));
        } else {
            writer.printText("doclet.navField");
        }
    }

    protected void printNavDetailLink(boolean link) {
        if (link) {
            writer.printHyperLink("", "field_detail",
                configuration().getText("doclet.navField"));
        } else {
            writer.printText("doclet.navField");
        }
    }

	@Override
	public String summaryId() {
		return "field_summary";
	}

    @Override
    public void writeFieldDoc(FieldDoc field) {
        boolean hasDetails = field.inlineTags().length > 0
            || field.tags().length > 0;
        writer.div();
        String val = "member";
        if (field.tags("apilevel").length == 1) {
            Tag tag = field.tags("apilevel")[0];
            val += " api-"+tag.text();
        }
        if (field.tags("aspect").length == 1) {
            Tag tag = field.tags("aspect")[0];
            val += " aspect-" + tag.text();
        }
        if (hasDetails) {
            val += " collapsible";
        }
        writer.addAttribute("class", val);
        writeSignature(field);
        if (hasDetails) {
            writer.div();
            writer.addAttribute("class", "member-details");
            writeDeprecated(field);
            writeComments(field);
            writeTags(field);
            writer.closeTag();
        }
        writer.closeTag();
    }
}
