/*
 * Copyright (c) 1997, 2004, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.formats.html;


import java.io.*;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.taglets.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;

import com.sun.javadoc.*;

/**
 * Writes method documentation in HTML format.
 *
 * @author Robert Field
 * @author Atul M Dambalkar
 * @author Jamie Ho (rewrite)
 */
public class AttributeWriterImpl extends AbstractExecutableMemberWriter
        implements AttributeWriter, MemberSummaryWriter {

    /**
     * Construct a new MethodWriterImpl.
     *
     * @param writer the writer for the class that the methods belong to.
     * @param classDoc the class being documented.
     */
    public AttributeWriterImpl(SubWriterHolderWriter writer, ClassDoc classDoc) {
        super(writer, classDoc);
    }

    /**
     * Construct a new MethodWriterImpl.
     *
     * @param writer The writer for the class that the methods belong to.
     */
    public AttributeWriterImpl(SubWriterHolderWriter writer) {
        super(writer);
    }

    /**
     * Write the inherited methods summary header for the given class.
     *
     * @param classDoc the class the summary belongs to.
     */
    public void writeInheritedMemberSummaryHeader(ClassDoc classDoc) {
        writer.printInheritedSummaryHeader(this, classDoc);
    }

    /**
     * {@inheritDoc}
     */
    public void writeInheritedMemberSummary(ClassDoc classDoc,
        ProgramElementDoc attribute, boolean isFirst, boolean isLast) {
        writer.div();
        String val = "inherited-member";
        if (attribute.tags("apilevel").length == 1) {
            Tag tag = attribute.tags("apilevel")[0];
            val += " api-"+tag.text();
        }
        writer.attr("class", val);
        writer.printInheritedSummaryMember(this, classDoc, attribute);
        writer.closeTag();
    }

    /**
     * Write the header for the method documentation.
     *
     * @param classDoc the class that the methods belong to.
     */
    public void writeHeader(ClassDoc classDoc, String header) {
        writer.anchor("attribute_details");
        writer.h2(header);
    }

    /**
     * Write the signature for the given method.
     *
     * @param method the method being documented.
     */
    public void writeSignature(MethodDoc method) {
        writer.displayLength = 0;
        writer.div();
        writer.addAttribute("class", "member-heading");
        String erasureAnchor;
        if ((erasureAnchor = getErasureAnchor(method)) != null) {
            writer.anchor(erasureAnchor);
        }
        writer.anchor(method);
        writer.pre();
        writer.writeAnnotationInfo(method);
        writer.bold(method.tags("attribute")[0].text());
        writer.space();
        //printModifiers(method);
        writeTypeParameters(method);
        printReturnType(method);
        if (configuration().linksource) {
            writer.printSrcLink(method, method.name());
        } else {
            bold(method.name());
        }
        writeParameters(method);
        writeExceptions(method);
        writer.closeTag();
        writer.closeTag();
    }

    /**
     * Write the deprecated output for the given method.
     *
     * @param method the method being documented.
     */
    public void writeDeprecated(MethodDoc method) {
        String output = ((TagletOutputImpl)
            (new DeprecatedTaglet()).getTagletOutput(method,
            writer.getTagletWriterInstance(false))).toString();
        if (output != null && output.trim().length() > 0) {
            writer.print(output);
        }
    }

    /**
     * Write the comments for the given method.
     *
     * @param method the method being documented.
     */
    public void writeComments(Type holder, MethodDoc method) {
        ClassDoc holderClassDoc = holder.asClassDoc();
        if (method.inlineTags().length > 0) {
            if (holder.asClassDoc().equals(classdoc) ||
                (! (holderClassDoc.isPublic() ||
                    Util.isLinkable(holderClassDoc, configuration())))) {
                writer.dd();
                writer.printInlineComment(method);
            } else {
                String classlink = writer.codeText(
                    writer.getDocLink(LinkInfoImpl.CONTEXT_METHOD_DOC_COPY,
                        holder.asClassDoc(), method,
                        holder.asClassDoc().isIncluded() ?
                            holder.typeName() : holder.qualifiedTypeName(),
                        false));
                writer.dd();
                writer.boldText(holder.asClassDoc().isClass()?
                        "doclet.Description_From_Class":
                        "doclet.Description_From_Interface",
                    classlink);
                writer.ddEnd();
                writer.dd();
                writer.printInlineComment(method);
            }
        }
    }

    /**
     * Write the tag output for the given method.
     *
     * @param method the method being documented.
     */
    public void writeTags(MethodDoc method) {
        writer.printTags(method);
    }

    /**
     * Write the method footer.
     */
    public void writeMethodFooter() {
        writer.ddEnd();
        writer.dlEnd();
    }

    /**
     * Write the footer for the method documentation.
     *
     * @param classDoc the class that the methods belong to.
     */
    public void writeFooter(ClassDoc classDoc) {
        //No footer to write for method documentation
    }

    /**
     * Close the writer.
     */
    public void close() throws IOException {
        writer.close();
    }

    public int getMemberKind() {
        return VisibleMemberMap.METHODS;
    }

    public String summaryLabel(ClassDoc cd) {
        return writer.getText("doclet.Attribute_Summary");
    }
    
    public void printSummaryLabel(ClassDoc cd) {
        writer.boldText("doclet.Attribute_Summary");
    }

    public void printSummaryAnchor(ClassDoc cd) {
        writer.anchor("attribute_summary");
    }

    public void printInheritedSummaryAnchor(ClassDoc cd) {
        writer.anchor("attributes_inherited_from_class_" +
            ConfigurationImpl.getInstance().getClassName(cd));
    }

    public void printInheritedSummaryLabel(ClassDoc cd) {
        String classlink = writer.getPreQualifiedClassLink(
            LinkInfoImpl.CONTEXT_MEMBER, cd, false);
        writer.bold();
        String key = cd.isClass()?
            "doclet.Attributes_Inherited_From_Class" :
            "doclet.Attributes_Inherited_From_Interface";
        writer.printText(key, classlink);
        writer.boldEnd();
    }

    /**
     * Appears in the attribute details list
     */
    protected void printSummaryType(ProgramElementDoc member) {
        MethodDoc meth = (MethodDoc)member;
        printModifierAndType(meth, meth.returnType());
    }

    protected static void printOverridden(HtmlDocletWriter writer,
            Type overriddenType, MethodDoc method) {
        if(writer.configuration.nocomment){
            return;
        }
        ClassDoc holderClassDoc = overriddenType.asClassDoc();
        if (! (holderClassDoc.isPublic() ||
            Util.isLinkable(holderClassDoc, writer.configuration()))) {
            //This is an implementation detail that should not be documented.
            return;
        }
        if (overriddenType.asClassDoc().isIncluded() && ! method.isIncluded()) {
            //The class is included but the method is not.  That means that it
            //is not visible so don't document this.
            return;
        }
        String label = "doclet.Overrides";
        int context = LinkInfoImpl.CONTEXT_METHOD_OVERRIDES;

        if (method != null) {
            if(overriddenType.asClassDoc().isAbstract() && method.isAbstract()){
                //Abstract method is implemented from abstract class,
                //not overridden
                label = "doclet.Specified_By";
                context = LinkInfoImpl.CONTEXT_METHOD_SPECIFIED_BY;
            }
            String overriddenTypeLink = writer.codeText(
                writer.getLink(new LinkInfoImpl(context, overriddenType)));
            String name = method.name();
            writer.dt();
            writer.boldText(label);
            writer.dd();
            String methLink = writer.codeText(
                writer.getLink(
                    new LinkInfoImpl(LinkInfoImpl.CONTEXT_MEMBER,
                        overriddenType.asClassDoc(),
                        writer.getAnchor(method), name, false)
                ));
            writer.printText("doclet.in_class", methLink, overriddenTypeLink);
        }
    }

    /**
     * Parse the &lt;Code&gt; tag and return the text.
     */
    protected String parseCodeTag(String tag){
        if(tag == null){
            return "";
        }

        String lc = tag.toLowerCase();
        int begin = lc.indexOf("<code>");
        int end = lc.indexOf("</code>");
        if(begin == -1 || end == -1 || end <= begin){
            return tag;
        } else {
            return tag.substring(begin + 6, end);
        }
    }

    protected static void printImplementsInfo(HtmlDocletWriter writer,
            MethodDoc method) {
        if(writer.configuration.nocomment){
            return;
        }
        ImplementedMethods implementedMethodsFinder =
            new ImplementedMethods(method, writer.configuration);
        MethodDoc[] implementedMethods = implementedMethodsFinder.build();
        for (int i = 0; i < implementedMethods.length; i++) {
            MethodDoc implementedMeth = implementedMethods[i];
            Type intfac = implementedMethodsFinder.getMethodHolder(implementedMeth);
            String methlink = "";
            String intfaclink = writer.codeText(
                writer.getLink(new LinkInfoImpl(
                    LinkInfoImpl.CONTEXT_METHOD_SPECIFIED_BY, intfac)));
            writer.dt();
            writer.boldText("doclet.Specified_By");
            writer.dd();
            methlink = writer.codeText(writer.getDocLink(
                LinkInfoImpl.CONTEXT_MEMBER, implementedMeth,
                implementedMeth.name(), false));
            writer.printText("doclet.in_interface", methlink, intfaclink);
        }

    }

    protected void printReturnType(MethodDoc method) {
        Type type = method.returnType();
        if (type != null) {
            writer.printLink(new LinkInfoImpl(LinkInfoImpl.CONTEXT_RETURN_TYPE,
                type));
            print(' ');
        }
    }

    protected void printNavSummaryLink(ClassDoc cd, boolean link) {
        if (link) {
            writer.printHyperLink("", (cd == null)?
                "attribute_summary":
                "attributes_inherited_from_class_" +
                ConfigurationImpl.getInstance().getClassName(cd),
                ConfigurationImpl.getInstance().getText("doclet.navMethod"));
        } else {
            writer.printText("doclet.navMethod");
        }
    }

    protected void printNavDetailLink(boolean link) {
        if (link) {
            writer.printHyperLink("", "attribute_details",
                ConfigurationImpl.getInstance().getText("doclet.navMethod"));
        } else {
            writer.printText("doclet.navMethod");
        }
    }

    @Override
    public String summaryId() {
        return "attribute_summary";
    }

    @Override
    public void writeAttributeDoc(MethodDoc attribute) {
        boolean hasDetails = attribute.inlineTags().length > 0
                || attribute.tags().length > 0;
        String val = "member";
        writer.div();
        if (attribute.tags("apilevel").length == 1) {
            Tag tag = attribute.tags("apilevel")[0];
            val += " api-" + tag.text();
        }
        if (attribute.tags("aspect").length == 1) {
            Tag tag = attribute.tags("aspect")[0];
            val += " aspect-" + tag.text();
        }
        if (hasDetails) {
            val += " collapsible";
        }
        writer.addAttribute("class", val);
        writeSignature(attribute);
        if (hasDetails) {
            writer.div();
            writer.addAttribute("class", "member-details");
            writeDeprecated(attribute);
            writeComments(attribute);
            writeTags(attribute);
            writer.closeTag();
        }
        writer.closeTag();
    }

    private void writeComments(MethodDoc attribute) {
        if (!writer.configuration.nocomment) {
            if (attribute.inlineTags().length == 0) {
                DocFinder.Output docs = DocFinder.search(new DocFinder.Input(
                        attribute));
                attribute = docs.inlineTags != null && docs.inlineTags.length > 0 ? (MethodDoc) docs.holder
                        : attribute;

            }
            // NOTE: When we fix the bug where ClassDoc.interfaceTypes() does
            // not pass all implemented interfaces, holder will be the
            // interface type. For now, it is really the erasure.
            writeComments(attribute.containingClass(), attribute);
        }
    }
}
