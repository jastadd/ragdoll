/*
 * Copyright (c) 1997, 2003, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.formats.html;

import com.sun.javadoc.*;

import java.util.*;
import java.io.*;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.taglets.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;

/**
 * Writes constructor documentation.
 *
 * @author Robert Field
 * @author Atul M Dambalkar
 */
public class ConstructorWriterImpl extends AbstractExecutableMemberWriter
    implements ConstructorWriter, MemberSummaryWriter {

    private boolean foundNonPubConstructor = false;

    /**
     * Construct a new ConstructorWriterImpl.
     *
     * @param writer The writer for the class that the constructors belong to.
     * @param classDoc the class being documented.
     */
    public ConstructorWriterImpl(SubWriterHolderWriter writer,
            ClassDoc classDoc) {
        super(writer, classDoc);
        VisibleMemberMap visibleMemberMap = new VisibleMemberMap(classDoc,
            VisibleMemberMap.CONSTRUCTORS, configuration().nodeprecated);
        List constructors = new ArrayList(visibleMemberMap.getMembersFor(classDoc));
        for (int i = 0; i < constructors.size(); i++) {
            if (((ProgramElementDoc)(constructors.get(i))).isProtected() ||
                ((ProgramElementDoc)(constructors.get(i))).isPrivate()) {
                setFoundNonPubConstructor(true);
            }
        }
    }

    /**
     * Construct a new ConstructorWriterImpl.
     *
     * @param writer The writer for the class that the constructors belong to.
     */
    public ConstructorWriterImpl(SubWriterHolderWriter writer) {
        super(writer);
    }

    /**
     * Write the header for the constructor documentation.
     *
     * @param classDoc the class that the constructors belong to.
     */
    public void writeHeader(ClassDoc classDoc, String header) {
        writer.anchor("constructor_detail");
        writer.h2(header);
    }

    /**
     * Write the signature for the given constructor.
     *
     * @param constructor the constructor being documented.
     */
    public void writeSignature(ConstructorDoc constructor) {
        writer.displayLength = 0;
        writer.div();
        writer.addAttribute("class", "member-heading");
        String erasureAnchor;
        if ((erasureAnchor = getErasureAnchor(constructor)) != null) {
            writer.anchor(erasureAnchor);
        }
        writer.anchor(constructor);
        writer.pre();
        writer.writeAnnotationInfo(constructor);
        printModifiers(constructor);
        //printReturnType((ConstructorDoc)constructor);
        if (configuration().linksource) {
            writer.printSrcLink(constructor, constructor.name());
        } else {
            bold(constructor.name());
        }
        writeParameters(constructor);
        writeExceptions(constructor);
        writer.closeTag();
        writer.closeTag();
    }

    /**
     * Write the deprecated output for the given constructor.
     *
     * @param constructor the constructor being documented.
     */
    public void writeDeprecated(ConstructorDoc constructor) {
        String output = ((TagletOutputImpl)
            (new DeprecatedTaglet()).getTagletOutput(constructor,
            writer.getTagletWriterInstance(false))).toString();
        if (output != null && output.trim().length() > 0) {
            writer.print(output);
        }
    }

    /**
     * Write the constructor footer.
     */
    public void writeConstructorDoc(ConstructorDoc constructor) {
        boolean hasDetails = constructor.inlineTags().length > 0
                || constructor.tags().length > 0;
        writer.div();
        String clss = "member";
        if (hasDetails)
            clss += " collapsible";
        writer.addAttribute("class", clss);
        writeSignature(constructor);
        // TODO: proper hasDetails!
        if (hasDetails) {
            writer.div();
            writer.addAttribute("class", "member-details");
            writeDeprecated(constructor);
            writer.printInlineComment(constructor);
            writer.printTags(constructor);
            writer.closeTag();
        }
        writer.closeTag();
    }

    /**
     * Close the writer.
     */
    public void close() throws IOException {
        writer.close();
    }

    /**
     * Let the writer know whether a non public constructor was found.
     *
     * @param foundNonPubConstructor true if we found a non public constructor.
     */
    public void setFoundNonPubConstructor(boolean foundNonPubConstructor) {
        this.foundNonPubConstructor = foundNonPubConstructor;
    }
    
    public String summaryLabel(ClassDoc cd) {
    	return writer.getText("doclet.Constructor_Summary");
    }

    public void printSummaryLabel(ClassDoc cd) {
        writer.boldText("doclet.Constructor_Summary");
    }

    public void printSummaryAnchor(ClassDoc cd) {
        writer.anchor("constructor_summary");
    }

    public void printInheritedSummaryAnchor(ClassDoc cd) {
    }   // no such

    public void printInheritedSummaryLabel(ClassDoc cd) {
        // no such
    }

    public int getMemberKind() {
        return VisibleMemberMap.CONSTRUCTORS;
    }

    protected void navSummaryLink(List members) {
        printNavSummaryLink(classdoc,
                members.size() > 0? true: false);
    }

    protected void printNavSummaryLink(ClassDoc cd, boolean link) {
        if (link) {
            writer.printHyperLink("", "constructor_summary",
                    ConfigurationImpl.getInstance().getText("doclet.navConstructor"));
        } else {
            writer.printText("doclet.navConstructor");
        }
    }

    protected void printNavDetailLink(boolean link) {
        if (link) {
            writer.printHyperLink("", "constructor_detail",
                    ConfigurationImpl.getInstance().getText("doclet.navConstructor"));
        } else {
            writer.printText("doclet.navConstructor");
        }
    }

    protected void printSummaryType(ProgramElementDoc member) {
        if (foundNonPubConstructor) {
            if (member.isProtected()) {
                print("protected ");
            } else if (member.isPrivate()) {
                print("private ");
            } else if (member.isPublic()) {
                writer.space();
            } else {
                writer.printText("doclet.Package_private");
            }
        }
    }

    /**
     * Write the inherited member summary header for the given class.
     *
     * @param classDoc the class the summary belongs to.
     */
    public void writeInheritedMemberSummaryHeader(ClassDoc classDoc) {
    }

    /**
     * {@inheritDoc}
     */
    public void writeInheritedMemberSummary(ClassDoc classDoc,
        ProgramElementDoc member, boolean isFirst, boolean isLast) {
        throw new UnsupportedOperationException();
    }

    /**
     * Write the inherited member summary footer for the given class.
     *
     * @param classDoc the class the summary belongs to.
     */
    public void writeInheritedMemberSummaryFooter(ClassDoc classDoc) {}

	@Override
	public String summaryId() {
		return "constructor_summary";
	}
}
