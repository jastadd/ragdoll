/*
 * Copyright (c) 2003, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.internal.toolkit.builders;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;
import ragdoll.tools.doclets.formats.html.*;

import com.sun.javadoc.*;

/**
 * The factory for constructing builders.
 *
 * This code is not part of an API.
 * It is implementation that is subject to change.
 * Do not use it as an API
 *
 * @author Jamie Ho
 * @since 1.4
 */

public class BuilderFactory {

    /**
     * The current configuration of the doclet.
     */
    private Configuration configuration;

    /**
     * Construct a builder factory using the given configuration.
     * @param configuration the configuration for the current doclet
     * being executed.
     */
    public BuilderFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Return the builder that builds the constant summary.
     * @return the builder that builds the constant summary.
     */
    public ConstantsSummaryBuilder getConstantsSummaryBuilder() throws Exception {
        return ConstantsSummaryBuilder.getInstance(configuration,
            new ConstantsSummaryWriterImpl(ConfigurationImpl.getInstance()));
    }

    /**
     * Return the builder that builds the package summary.
     *
     * @param pkg the package being documented.
     * @param prevPkg the previous package being documented.
     * @param nextPkg the next package being documented.
     * @return the builder that builds the constant summary.
     */
    public PackageSummaryBuilder getPackageSummaryBuilder(PackageDoc pkg, PackageDoc prevPkg,
            PackageDoc nextPkg) throws Exception {
        return PackageSummaryBuilder.getInstance(configuration, pkg,
            new PackageWriterImpl(ConfigurationImpl.getInstance(), pkg,
                prevPkg, nextPkg));
    }

    /**
     * Return the builder for the class.
     *
     * @param classDoc the class being documented.
     * @param prevClass the previous class that was documented.
     * @param nextClass the next class being documented.
     * @param classTree the class tree.
     * @return the writer for the class.  Return null if this
     * writer is not supported by the doclet.
     */
    public ClassBuilder getClassBuilder(ClassDoc classDoc,
        ClassDoc prevClass, ClassDoc nextClass, ClassTree classTree)
            throws Exception {
        return ClassBuilder.getInstance(configuration, classDoc,
            new ClassWriterImpl(classDoc, prevClass, nextClass, classTree));
    }

    /**
     * Return the builder for the annotation type.
     *
     * @param annotationType the annotation type being documented.
     * @param prevType the previous type that was documented.
     * @param nextType the next type being documented.
     * @return the writer for the annotation type.  Return null if this
     * writer is not supported by the doclet.
     */
    public AnnotationTypeBuilder getAnnotationTypeBuilder(
        AnnotationTypeDoc annotationType,
        Type prevType, Type nextType)
            throws Exception {
        return AnnotationTypeBuilder.getInstance(configuration, annotationType,
            new AnnotationTypeWriterImpl(annotationType, prevType, nextType));
    }

    /**
     * Return an instance of the method builder for the given class.
     *
     * @return an instance of the method builder for the given class.
     */
    public MethodBuilder getMethodBuilder(ClassWriter classWriter)
           throws Exception {
        return MethodBuilder.getInstance(configuration,
            classWriter.getClassDoc(),
            new MethodWriterImpl((SubWriterHolderWriter) classWriter,
                classWriter.getClassDoc()));
    }
    
    public AttributeBuilder getAttributeBuilder(ClassWriter classWriter)
        throws Exception {
        return AttributeBuilder.getInstance(configuration,
            classWriter.getClassDoc(),
            new AttributeWriterImpl((SubWriterHolderWriter) classWriter,
                classWriter.getClassDoc()));
    }

    /**
     * Return an instance of the annotation type member builder for the given
     * class.
     *
     * @return an instance of the annotation type memebr builder for the given
     *         annotation type.
     */
    public AnnotationTypeOptionalMemberBuilder getAnnotationTypeOptionalMemberBuilder(
            AnnotationTypeWriter annotationTypeWriter)
    throws Exception {
        return AnnotationTypeOptionalMemberBuilder.getInstance(configuration,
            annotationTypeWriter.getAnnotationTypeDoc(),
            new AnnotationTypeOptionalMemberWriterImpl(
                (SubWriterHolderWriter) annotationTypeWriter,
                annotationTypeWriter.getAnnotationTypeDoc()));
    }

    /**
     * Return an instance of the annotation type member builder for the given
     * class.
     *
     * @return an instance of the annotation type memebr builder for the given
     *         annotation type.
     */
    public AnnotationTypeRequiredMemberBuilder getAnnotationTypeRequiredMemberBuilder(
            AnnotationTypeWriter annotationTypeWriter)
    throws Exception {
        return AnnotationTypeRequiredMemberBuilder.getInstance(configuration,
            annotationTypeWriter.getAnnotationTypeDoc(),
            new AnnotationTypeRequiredMemberWriterImpl(
                (SubWriterHolderWriter) annotationTypeWriter,
                annotationTypeWriter.getAnnotationTypeDoc()));
    }

    /**
     * Return an instance of the enum constants builder for the given class.
     *
     * @return an instance of the enum constants builder for the given class.
     */
    public EnumConstantBuilder getEnumConstantsBuilder(ClassWriter classWriter)
            throws Exception {
        return EnumConstantBuilder.getInstance(configuration, classWriter.getClassDoc(),
            new EnumConstantWriterImpl((SubWriterHolderWriter) classWriter,
                classWriter.getClassDoc()));
    }

    /**
     * Return an instance of the field builder for the given class.
     *
     * @return an instance of the field builder for the given class.
     */
    public FieldBuilder getFieldBuilder(ClassWriter classWriter)
            throws Exception {
        return FieldBuilder.getInstance(configuration, classWriter.getClassDoc(),
            new FieldWriterImpl((SubWriterHolderWriter) classWriter,
                classWriter.getClassDoc()));
    }

    /**
     * Return an instance of the constructor builder for the given class.
     *
     * @return an instance of the constructor builder for the given class.
     */
    public ConstructorBuilder getConstructorBuilder(ClassWriter classWriter)
            throws Exception {
        return ConstructorBuilder.getInstance(configuration, classWriter.getClassDoc(),
            new ConstructorWriterImpl((SubWriterHolderWriter) classWriter,
                classWriter.getClassDoc()));
    }

    /**
     * Return an instance of the member summary builder for the given class.
     *
     * @return an instance of the member summary builder for the given class.
     */
    public MemberSummaryBuilder getMemberSummaryBuilder(ClassWriter classWriter)
            throws Exception {
        return MemberSummaryBuilder.getInstance(classWriter, configuration);
    }

    /**
     * Return an instance of the member summary builder for the given annotation
     * type.
     *
     * @return an instance of the member summary builder for the given
     *         annotation type.
     */
    public MemberSummaryBuilder getMemberSummaryBuilder(
            AnnotationTypeWriter annotationTypeWriter)
    throws Exception {
        return MemberSummaryBuilder.getInstance(annotationTypeWriter,
            configuration);
    }

    /**
     * Return the builder that builds the serialized form.
     *
     * @return the builder that builds the serialized form.
     */
    public SerializedFormBuilder getSerializedFormBuilder()
            throws Exception {
        return SerializedFormBuilder.getInstance(configuration);
    }
}
