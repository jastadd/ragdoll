/*
 * Copyright (c) 2003, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.internal.toolkit.builders;

import com.sun.javadoc.*;

import java.util.*;
import java.lang.reflect.*;

import ragdoll.tools.doclets.formats.html.FieldWriterImpl;
import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;

/**
 * Builds documentation for a field.
 *
 * This code is not part of an API.
 * It is implementation that is subject to change.
 * Do not use it as an API
 *
 * @author Jamie Ho
 * @since 1.5
 */
public class FieldBuilder extends AbstractMemberBuilder {

    /**
     * The class whose fields are being documented.
     */
    private ClassDoc classDoc;

    /**
     * The visible fields for the given class.
     */
    private VisibleMemberMap visibleMemberMap;

    /**
     * The writer to output the field documentation.
     */
    private FieldWriterImpl writer;

    /**
     * The list of fields being documented.
     */
    private List fields;

    /**
     * The index of the current field that is being documented at this point
     * in time.
     */
    private int currentFieldIndex;

    /**
     * Construct a new FieldBuilder.
     *
     * @param configuration the current configuration of the
     *                      doclet.
     */
    private FieldBuilder(Configuration configuration) {
        super(configuration);
    }

    /**
     * Construct a new FieldBuilder.
     *
     * @param configuration the current configuration of the doclet.
     * @param classDoc the class whoses members are being documented.
     * @param writer the doclet specific writer.
     */
    public static FieldBuilder getInstance(Configuration configuration,
            ClassDoc classDoc, FieldWriterImpl writer) {
        FieldBuilder builder = new FieldBuilder(configuration);
        builder.classDoc = classDoc;
        builder.writer = writer;
        builder.visibleMemberMap = new VisibleMemberMap(classDoc,
                VisibleMemberMap.FIELDS, configuration.nodeprecated);
        builder.fields = new ArrayList(
                builder.visibleMemberMap.getLeafClassMembers(configuration));
        if (configuration.getMemberComparator() != null) {
            Collections.sort(builder.fields,
                    configuration.getMemberComparator());
        }
        return builder;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return "FieldDetails";
    }

    /**
     * {@inheritDoc}
     */
    public void invokeMethod(
            String methodName,
            Class[] paramClasses,
            Object[] params)
        throws Exception {
        if (DEBUG) {
            configuration.root.printError(
                    "DEBUG: " + this.getClass().getName() + "." + methodName);
        }
        Method method = this.getClass().getMethod(methodName, paramClasses);
        method.invoke(this, params);
            }

    /**
     * Returns a list of fields that will be documented for the given class.
     * This information can be used for doclet specific documentation
     * generation.
     *
     * @param classDoc the {@link ClassDoc} we want to check.
     * @return a list of fields that will be documented.
     */
    public List members(ClassDoc classDoc) {
        return visibleMemberMap.getMembersFor(classDoc);
    }

    /**
     * Returns the visible member map for the fields of this class.
     *
     * @return the visible member map for the fields of this class.
     */
    public VisibleMemberMap getVisibleMemberMap() {
        return visibleMemberMap;
    }

    public Set<String> aspects() {
        Set<String> aspects = new HashSet<String>();
        for (currentFieldIndex = 0;
                currentFieldIndex < fields.size();
                currentFieldIndex++) {
            FieldDoc field = (FieldDoc) fields.get(currentFieldIndex);
            if (field.tags("aspect").length == 1) {
                Tag tag = field.tags("aspect")[0];
                aspects.add(tag.text());
            }
        }
        return aspects;
    }
    
    /**
     * summaryOrder.size()
     */
    public boolean hasMembersToDocument() {
        return fields.size() > 0;
    }

    /**
     * Build the field documentation.
     */
    public void buildFieldDoc() {
        if (writer == null) {
            return;
        }
        writer.getWriter().div();
        writer.getWriter().attr("class", "content");
        for (currentFieldIndex = 0;
                currentFieldIndex < fields.size();
                currentFieldIndex++) {
            writer.writeFieldDoc((FieldDoc) fields.get(currentFieldIndex));
        }
        writer.getWriter().closeTag();
    }
    
    /**
     * Build the overall header.
     */
    public void buildHeader() {
        writer.getWriter().div();
        writer.getWriter().attr("id", "field-heading");
        writer.getWriter().attr("class", "heading");
        writer.writeHeader(
                classDoc,
                configuration.getText("doclet.Field_Detail"));
        writer.getWriter().closeTag();
    }
    
    /**
     * Build the overall footer.
     */
    public void buildFooter() {
        writer.writeFooter(classDoc);
    }

    /**
     * Return the field writer for this builder.
     *
     * @return the field writer for this builder.
     */
    public FieldWriter getWriter() {
        return writer;
    }
}
