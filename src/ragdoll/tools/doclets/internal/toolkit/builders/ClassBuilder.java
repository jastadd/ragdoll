/*
 * Copyright (c) 2003, 2004, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.internal.toolkit.builders;

import com.sun.javadoc.*;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

import ragdoll.tools.doclets.internal.toolkit.*;
import ragdoll.tools.doclets.internal.toolkit.util.*;
import ragdoll.tools.doclets.formats.html.*;

/**
 * Builds the summary for a given class.
 *
 * This code is not part of an API.
 * It is implementation that is subject to change.
 * Do not use it as an API
 *
 * @author Jamie H
 * @author Jesper &Ouml;qvist
 * @since 1.5
 */
public class ClassBuilder extends AbstractBuilder {

    /**
     * The class being documented.
     */
    private ClassDoc classDoc;

    /**
     * The doclet specific writer.
     */
    private ClassWriterImpl writer;

    /**
     * Keep track of whether or not this classdoc is an interface.
     */
    private boolean isInterface = false;

    /**
     * Keep track of whether or not this classdoc is an enum.
     */
    private boolean isEnum = false;

    /**
     * Construct a new ClassBuilder.
     *
     * @param configuration the current configuration of the
     *                      doclet.
     */
    private ClassBuilder(Configuration configuration) {
        super(configuration);
    }

    /**
     * Construct a new ClassBuilder.
     *
     * @param configuration the current configuration of the doclet.
     * @param classDoc the class being documented.
     * @param writer the doclet specific writer.
     */
    public static ClassBuilder getInstance(Configuration configuration,
        ClassDoc classDoc, ClassWriterImpl writer)
    throws Exception {
        ClassBuilder builder = new ClassBuilder(configuration);
        builder.configuration = configuration;
        builder.classDoc = classDoc;
        builder.writer = writer;
        if (classDoc.isInterface()) {
            builder.isInterface = true;
        } else if (classDoc.isEnum()) {
            builder.isEnum = true;
            Util.setEnumDocumentation(configuration, classDoc);
        }
        if(containingPackagesSeen == null) {
            containingPackagesSeen = new HashSet<String>();
        }
        return builder;
    }

    /**
     * {@inheritDoc}
     */
    public void invokeMethod(String methodName, Class<?>[] paramClasses,
            Object[] params)
    throws Exception {
        if (DEBUG) {
            configuration.root.printError("DEBUG: " + this.getClass().getName()
                + "." + methodName);
        }
        Method method = this.getClass().getMethod(methodName, paramClasses);
        method.invoke(this, params);
    }

    /**
     * {@inheritDoc}
     */
    public void build() {
        try {

            String key;
            if (isInterface) {
                key =  "doclet.Interface";
            } else if (isEnum) {
                key = "doclet.Enum";
            } else {
                if (classDoc.tags("ast").length == 0)
                    key = "doclet.Class";
                else {
                    Tag tag = classDoc.tags("ast")[0];
                    if (tag.text().equals("node"))
                        key = "doclet.ASTNode";
                    else if (tag.text().equals("interface"))
                        key = "doclet.ASTInterface";
                    else
                        key = "doclet.ASTClass";
                }
            }

            writer.writeHtmlHeader();
            writer.div();
            writer.addAttribute("id", "page");
            writer.writeHeader(configuration.getText(key) + " " + classDoc.name());

            writer.div();
            writer.addAttribute("class", "partition");
            writer.div();
            writer.addAttribute("class", "heading");
            writer.h2("General Info");
            writer.closeTag();
            writer.div();
            writer.addAttribute("class", "content");
            if (classDoc.tags("production").length == 1) {
                writer.print(writer.getTagletWriterInstance(false).productionTagOutput(classDoc.tags("production")[0]).toString());
            }
            writer.writeClassDeprecationInfo();

            StringBuffer modifiers = new StringBuffer(classDoc.modifiers() + " ");
            if (isEnum) {
                modifiers.append("enum ");
                int index;
                if ((index = modifiers.indexOf("abstract")) >= 0) {
                    modifiers.delete(index, index + (new String("abstract")).length());
                    modifiers = new StringBuffer(
                            Util.replaceText(modifiers.toString(), "  ", " "));
                }
                if ((index = modifiers.indexOf("final")) >= 0) {
                    modifiers.delete(index, index + (new String("final")).length());
                    modifiers = new StringBuffer(
                            Util.replaceText(modifiers.toString(), "  ", " "));
                }
            //} else if (classDoc.isAnnotationType()) {
                //modifiers.append("@interface ");
            } else if (! isInterface) {
                modifiers.append("class ");
            }
            //writer.writeClassSignature(modifiers.toString());

            //writer.h3("References:");
            if (classDoc.isClass()) {
                writer.print("<dl>");
                writer.print("<dt><strong>Type Hierarchy:</strong></dt>");
                writer.print("<dd>");
                writer.writeClassTree();
                writer.print("</dd>");
                writer.print("</dl>");
            }
            writer.writeTypeParamInfo();
            writer.writeSuperInterfacesInfo();
            writer.writeImplementedInterfacesInfo();
            writer.writeSubClassInfo();
            writer.writeSubInterfacesInfo();
            writer.writeInterfaceUsageInfo();
            writer.writeNestedClassInfo();
            configuration.getBuilderFactory().
                getMemberSummaryBuilder(writer).buildNestedClassesSummary();
            configuration.getBuilderFactory().
                getMemberSummaryBuilder(writer).buildNestedClassesInheritedSummary();
            writer.closeTag();
            writer.closeTag();

            writer.div();
            writer.addAttribute("class", "partition");
            writer.div();
            writer.addAttribute("class", "heading");
            writer.h2("Description");
            writer.closeTag();
            writer.div();
            writer.addAttribute("class", "content");
            writer.writeClassDescription();
            writer.writeClassTagInfo();
            writer.closeTag();
            writer.closeTag();

            ConstructorBuilder constructorBuilder = configuration.getBuilderFactory().
                    getConstructorBuilder(writer);
            AttributeBuilder attributeBuilder = configuration.getBuilderFactory().
                    getAttributeBuilder(writer);
            MethodBuilder methodBuilder = configuration.getBuilderFactory().
                    getMethodBuilder(writer);
            FieldBuilder fieldBuilder = configuration.getBuilderFactory().
                    getFieldBuilder(writer);
            EnumConstantBuilder enumConstantBuilder = configuration.getBuilderFactory().
                    getEnumConstantsBuilder(writer);
            
            // set of aspects
            Set<String> aspects = attributeBuilder.aspects();
            aspects.addAll(methodBuilder.aspects());
            aspects.addAll(fieldBuilder.aspects());
            
            writer.div();
            writer.attr("class", "partition");
            writer.div();
            writer.attr("id", "filter-heading");
            writer.attr("class", "heading");
            writer.h2("Member Filters");
            writer.closeTag();
            writer.div();
            writer.attr("class", "content");
            writer.print("<form id=\"filter\">"+
                    "<button id=\"hide-all\" type=\"button\">Hide All</button> <button id=\"show-all\" type=\"button\">Show All</button>" +
                    "<p>API level: "+
                    "<div class=\"filter\"><label><input id=\"api-high-level\" type=\"checkbox\" checked=\"checked\"/>high-level</label></div> " +
                    "<div class=\"filter\"><label><input id=\"api-low-level\" type=\"checkbox\"/>low-level</label></div> " +
                    "<div class=\"filter\"><label><input id=\"api-internal\" type=\"checkbox\"/>internal-level</label></div>" +
                    "<div style=\"clear:both\"></div>" +
                    "<p>Aspect:");
            if (!aspects.isEmpty()) {
                for (String aspect : aspects) {
                    writer.print(" <div class=\"filter\"><label><input id=\"aspect-" + aspect +
                            "\" type=\"checkbox\" checked=\"checked\"/>" +
                            aspect + "</label></div>");
                }
            }
            writer.print("<div style=\"clear:both\"></div>");
            writer.print("</form>");
            writer.closeTag();
            writer.closeTag();

            if (constructorBuilder.hasMembersToDocument()) {
                writer.div();
                writer.addAttribute("class", "partition");
                constructorBuilder.buildHeader();
                constructorBuilder.buildConstructorDoc();
                writer.closeTag();
            }

            if (attributeBuilder.hasMembersToDocument()) {
                writer.div();
                writer.addAttribute("class", "partition");
                attributeBuilder.buildHeader();
                attributeBuilder.buildAttributeDoc();
                attributeBuilder.buildFooter();
                writer.closeTag();
            }

            if (methodBuilder.hasMembersToDocument()) {
                writer.div();
                writer.addAttribute("class", "partition");
                methodBuilder.buildHeader();
                methodBuilder.buildMethodDoc();
                methodBuilder.buildFooter();
                writer.closeTag();
            }

            if (fieldBuilder.hasMembersToDocument()) {
                writer.div();
                writer.addAttribute("class", "partition");
                fieldBuilder.buildHeader();
                fieldBuilder.buildFieldDoc();
                fieldBuilder.buildFooter();
                writer.closeTag();
            }

            if (enumConstantBuilder.hasMembersToDocument()) {
                writer.div();
                writer.addAttribute("class", "partition");
                enumConstantBuilder.buildHeader();
                enumConstantBuilder.buildEnumConstant();
                enumConstantBuilder.buildFooter();
                writer.closeTag();
            }

            MemberSummaryBuilder memberSummaryBuilder = configuration.getBuilderFactory().
                getMemberSummaryBuilder(writer);
            if (memberSummaryBuilder.hasMembersToDocument()) {
                writer.div();
                writer.addAttribute("class", "partition");
                writer.div();
                writer.attr("class", "heading");
                writer.h2("Inherited Members");
                writer.closeTag();
                writer.div();
                writer.attr("class", "content");
                memberSummaryBuilder.buildAttributesInheritedSummary();
                memberSummaryBuilder.buildMethodsInheritedSummary();
                memberSummaryBuilder.buildFieldsInheritedSummary();
                writer.closeTag();
                writer.closeTag();
            }

            writer.closeTag();
            writer.writeFooter();

            writer.close();
            copyDocFiles();

        } catch (Exception e) {
            e.printStackTrace();
            throw new DocletAbortException();
        }
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return "ClassDoc";
    }

    /**
     * Copy the doc files for the current ClassDoc if necessary.
     */
    private void copyDocFiles() {
        PackageDoc containingPackage = classDoc.containingPackage();
        if((configuration.packages == null ||
                    Arrays.binarySearch(configuration.packages,
                        containingPackage) < 0) &&
                ! containingPackagesSeen.contains(containingPackage.name())){
            //Only copy doc files dir if the containing package is not
            //documented AND if we have not documented a class from the same
            //package already. Otherwise, we are making duplicate copies.
            Util.copyDocFiles(configuration,
                    Util.getPackageSourcePath(configuration,
                        classDoc.containingPackage()) +
                    DirectoryManager.getDirectoryPath(classDoc.containingPackage())
                    + File.separator, DocletConstants.DOC_FILES_DIR_NAME, true);
            containingPackagesSeen.add(containingPackage.name());
            }
    }

}
