/*
 * Copyright (c) 2001, 2006, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package ragdoll.tools.doclets.internal.toolkit.util;

import java.io.*;
import java.util.*;

import ragdoll.tools.doclets.internal.toolkit.*;

import com.sun.javadoc.*;

/**
 * Converts Java Source Code to HTML.
 *
 * This code is not part of an API.
 * It is implementation that is subject to change.
 * Do not use it as an API
 *
 * @author Jamie Ho
 * @since 1.4
 */
public class SourceToHTMLConverter {

    /**
     * The background color.
     */
    protected static final String BGCOLOR = "white";

    /**
     * The line number color.
     */
    protected static final String LINE_NO_COLOR = "green";

    /**
     * Source is converted to HTML using static methods below.
     */
    private SourceToHTMLConverter() {}

    /**
     * Convert the Classes in the given RootDoc to HTML.
     * @param configuration the configuration.
     * @param rd the RootDoc to convert.
     * @param outputdir the name of the directory to output to.
     */
    public static void convertRoot(Configuration configuration, RootDoc rd, String outputdir) {
        if (rd == null || outputdir == null) {
            return;
        }
        Set<String> ragFiles = new HashSet<String>();
        PackageDoc[] pds = rd.specifiedPackages();
        for (int i = 0; i < pds.length; i++) {
            convertPackage(configuration, pds[i], outputdir, ragFiles);
        }
        ClassDoc[] cds = rd.specifiedClasses();
        for (int i = 0; i < cds.length; i++) {
            convertClass(configuration, cds[i],
                getPackageOutputDir(outputdir, cds[i].containingPackage()),
                ragFiles);
        }
        for (String ragFile : ragFiles)
            convertRagFile(configuration, ragFile);
    }

    public static String getRagHtmlOutputDir(Configuration configuration, String ragFile) {
        String path = configuration.getRelativePath(ragFile);
        if (path.lastIndexOf(".."+File.separator) > -1)
            path = path.substring(path.lastIndexOf(".."+File.separator)+3);
        if (path.lastIndexOf(File.separator) > -1)
            path = path.substring(0, path.lastIndexOf(File.separator));
        return "rags-html" + File.separator + path;
    }

    public static String getRagHtmlName(String ragFile) {
        String ragName = ragFile;
        if (ragName.lastIndexOf(File.separator) > -1)
            ragName = ragName.substring(ragName.lastIndexOf(File.separator)+1);
        return ragName;
    }

    public static void convertRagFile(Configuration configuration, String ragFile) {
        try {
            String outputdir = configuration.destDirName + File.separator +
                getRagHtmlOutputDir(configuration, ragFile);
            String ragName = getRagHtmlName(ragFile);

            if (ragName.length() == 0) {
                System.err.println("Warning: Not a standard rag file name: "+ragFile);
                return;
            }

            File file = new File(ragFile);
            if (file == null) {
                return;
            }
            Reader r = new FileReader(file);
            LineNumberReader reader = new LineNumberReader(r);
            int lineno = 1;
            String line;
            StringBuffer output = new StringBuffer();
            try {
                while ((line = reader.readLine()) != null) {
                    output.append(formatLine(line, configuration.sourcetab, lineno));
                    lineno++;
                }
            } finally {
                reader.close();
            }
            output.insert(0, getHeader(configuration, outputdir));
            output.append(getFooter());
            writeToFile(output.toString(), outputdir, ragName, configuration);
        } catch (FileNotFoundException e) {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert the Classes in the given Package to HTML.
     * @param configuration the configuration.
     * @param pd the Package to convert.
     * @param outputdir the name of the directory to output to.
     */
    public static void convertPackage(Configuration configuration,
            PackageDoc pd, String outputdir, Set<String> ragFiles) {
        if (pd == null || outputdir == null) {
            return;
        }
        String classOutputdir = getPackageOutputDir(outputdir, pd);
        ClassDoc[] cds = pd.allClasses();
        for (int i = 0; i < cds.length; i++) {
            convertClass(configuration, cds[i], classOutputdir, ragFiles);
        }
    }

    /**
     * Return the directory to write output to for the given package.
     * @param outputDir the directory to output to.
     * @param pd the Package to generate output for.
     */
    private static String getPackageOutputDir(String outputDir, PackageDoc pd) {
        return outputDir + File.separator +
            DirectoryManager.getDirectoryPath(pd) + File.separator;
    }

    /**
     * Convert the given Class to HTML.
     * @param configuration the configuration.
     * @param cd the class to convert.
     * @param outputdir the name of the directory to output to.
     */
    public static void convertClass(Configuration configuration, ClassDoc cd,
            String outputdir, Set<String> ragFiles) {
        if (cd == null || outputdir == null) {
            return;
        }
        try {
            MethodDoc[] methods = cd.methods();
            for (MethodDoc method : methods) {
                Tag[] tags = method.tags("declaredat");
                if (tags.length == 1) {
                    String declaredat = tags[0].text();
                    int sep = declaredat.lastIndexOf(':');

                    String path;
                    if (sep != -1)
                        path = declaredat.substring(0, sep);
                    else
                        path = declaredat;
                    ragFiles.add(path);
                }
            }

            SourcePosition sp = cd.position();
            if (sp == null)
                return;
            Reader r;
            // temp hack until we can update SourcePosition API.
//            if (sp instanceof com.sun.tools.javadoc.SourcePositionImpl) {
//                FileObject fo = ((com.sun.tools.javadoc.SourcePositionImpl) sp).fileObject();
//                if (fo == null)
//                    return;
//                r = fo.openReader(true);
//            } else {
                File file = sp.file();
                if (file == null)
                    return;
                r = new FileReader(file);
//            }
            LineNumberReader reader = new LineNumberReader(r);
            int lineno = 1;
            String line;
            StringBuffer output = new StringBuffer();
            try {
                while ((line = reader.readLine()) != null) {
                    output.append(formatLine(line, configuration.sourcetab, lineno));
                    lineno++;
                }
            } finally {
                reader.close();
            }
            output.insert(0, getHeader(configuration, outputdir));
            output.append(getFooter());
            writeToFile(output.toString(), outputdir, cd.name(), configuration);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Write the output to the file.
     * @param output the string to output.
     * @param outputDir the directory to output to.
     * @param className the name of the class that I am converting to HTML.
     * @param configuration the Doclet configuration to pass notices to.
     */
    private static void writeToFile(String output, String outputDir, String className, Configuration configuration) throws IOException {
        File dir = new File(outputDir);
        dir.mkdirs();
        File newFile = new File(dir, className + ".html");
        configuration.message.notice("doclet.Generating_0", newFile.getPath());
        FileOutputStream fout = new FileOutputStream(newFile);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fout));
        bw.write(output);
        bw.close();
        fout.close();
    }

    /**
     * Get the header.
     * @return the header to the output file
     */
    protected static String getHeader(Configuration configuration, String outputdir) {
        File destDir = new File(configuration.destDirName);
        File outputDir = new File(outputdir);
        String relativePath = "";// path to doc root
        while (!outputDir.getName().equals(destDir.getName())) {
            outputDir = outputDir.getParentFile();
            relativePath += "../";
        }
        StringBuffer result = new StringBuffer("<HTML>" + DocletConstants.NL);
        result.append("<head>");
        result.append("<script src=\"" + relativePath + "resources/jquery.js\"></script>");
        result.append("<script>");
        result.append("$(document).ready(function(event) {" +
        		"$(document.location.hash).css('background', 'yellow');" +
        		"$(document.location.hash + ' span').css('color', 'red');" +
        		"});");
        result.append("</script>");
        result.append("</head>");
        result.append("<BODY BGCOLOR=\""+ BGCOLOR + "\">" + DocletConstants.NL);
        result.append("<PRE>" + DocletConstants.NL);
        return result.toString();
    }

    /**
     * Get the footer
     * @return the footer to the output file
     */
    protected static String getFooter() {
        StringBuffer footer = new StringBuffer();
        footer.append("</PRE>" + DocletConstants.NL + "</BODY>" +
            DocletConstants.NL + "</HTML>" + DocletConstants.NL);
        return footer.toString();
    }

    /**
     * Get the HTML for the lines.
     * @param lineno The line number
     * @return the HTML code for the line
     */
    protected static String getHTMLLineNo(int lineno) {
        StringBuffer result = new StringBuffer("<span style=\"color: " + LINE_NO_COLOR
            + "\">");
        if (lineno < 10) {
            result.append("00" + ((new Integer(lineno)).toString()));
        } else if (lineno < 100) {
            result.append("0" + ((new Integer(lineno)).toString()));
        } else {
            result.append((new Integer(lineno)).toString());
        }
        result.append("</span>    ");
        return result.toString();
    }

    /**
     * Format a given line of source. <br>
     * Note:  In the future, we will add special colors for constructs in the
     * language.
     * @param line the string to format.
     * @param tabLength the number of spaces for each tab.
     * @param currentLineNo the current number.
     */
    protected static String formatLine(String line, int tabLength, int currentLineNo) {
        if (line == null) {
            return null;
        }
        StringBuffer lineBuffer = new StringBuffer();
        //Insert an anchor for the line
        lineBuffer.append("<span id=\"line-"+currentLineNo + "\">");
        lineBuffer.append(getHTMLLineNo(currentLineNo));
        lineBuffer.append(Util.escapeHtmlChars(line));
        lineBuffer.append("<a name=\"line-" + Integer.toString(currentLineNo) + "\"></a>");
        lineBuffer.append("</span>");
        lineBuffer.append(DocletConstants.NL);
        Util.replaceTabs(tabLength, lineBuffer);
        return lineBuffer.toString();
    }

    /**
     * Given an array of <code>Doc</code>s, add to the given <code>HashMap</code> the
     * line numbers and anchors that should be inserted in the output at those lines.
     * @param docs the array of <code>Doc</code>s to add anchors for.
     * @param hash the <code>HashMap</code> to add to.
     */
    protected static void addToHash(Doc[] docs, HashMap<Integer,String> hash) {
        if(docs == null) {
            return;
        }
        for(int i = 0; i < docs.length; i++) {
            hash.put(docs[i].position().line(), getAnchor(docs[i]));
        }
    }

    /**
     * Given a <code>Doc</code>, return an anchor for it.
     * @param d the <code>Doc</code> to check.
     * @return an anchor of the form &lt;a name="my_name">&lt;/a>
     */
    protected static String getAnchor(Doc d) {
        return "    <a name=\"" + getAnchorName(d) + "\"></a>";
    }

    /**
     * Given a <code>Doc</code>, return an anchor name for it.
     * @param d the <code>Doc</code> to check.
     * @return the name of the anchor.
     */
    public static String getAnchorName(Doc d) {
        return getAnchorName(""+d.position().line());
    }
    
    public static String getAnchorName(String line) {
        return "line-" + line;
    }
}
