package ragdoll;

/**
 * Used to debug RagDoll using e.g. eclipse.
 * 
 * @author Jesper &Ouml;qvist
 *
 */
public class DebugRunner {

	public static void main(String[] args) {
		com.sun.tools.javadoc.Main.execute(new String[] {
				"-sourcepath", "src",
				"-docletpath", "bin",
				"-doclet", "ragdoll.RagDollDoclet",
				"-d", "doc",
				"-package", "ragdoll.html",
				"-ragroot", ".",
				"-linksource",
		});
	}
}
