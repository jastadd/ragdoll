package ragdoll;

/**
 * Entrypoint for RagDoll, if run from the jar archive.
 * 
 * @author Jesper \u00d6qvist
 */
public class RagDoll {
	public static void main(String[] args) {
		String[] args1 = new String[args.length + 2];
		System.arraycopy(args, 0, args1, 2, args.length);
		args1[0] = "-doclet";
		args1[1] = "ragdoll.RagDollDoclet";
		com.sun.tools.javadoc.Main.execute(args1);
	}
}
