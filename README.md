RagDoll
=======

RagDoll is a JavaDoc doclet tailored for JastAdd generated code.

Copyright & License
-------------------

RagDoll is based on the JavaDoc code from OpenJDK 6, copyright of Oracle Corp.

Copyright 2011-2013 Jesper &Ouml;qvist <jesper.oqvist@cs.lth.se>

RagDoll is licensed under the [GPLv2 license with the "classpath" exception][1]
as specified in the LICENSE file included in the source bundles.

Running
-------

The RagDoll doclet can be run with javadoc by saying something like

    $ javadoc -docletpath RagDoll.jar -doclet ragdoll.RagDollDoclet -sourcepath src -package package -d doc

The above example will create HTML documentation in the doc directory, based
on the classes in the ragdoll.html package, from the source directory src.

RagDoll can be used in an Apache Ant build script, using for example the following target:

    <target name="doc" depends="build" description="generate documentation">
        <mkdir dir="doc"/>
        <javadoc destdir="doc" docletpath="bin" doclet="ragdoll.RagDollDoclet">
            <arg value="-linksource"/>
            <arg value="-ragroot"/>
            <arg value="."/>
            <packageset dir="src" defaultexcludes="yes">
                <include name="ragdoll/html"/>
            </packageset>
        </javadoc>
    </target>

Requirements
------------

In order to output attribute information and such, you will need to generate
the Java code for your AST with a newer version of JastAdd2 which supports
the RagDoll tags for attributes.

Try using the jastadd2.jar included in the tools directory, it should be
backward compatible with older versions of JastAdd2 and provide the
RagDoll tags needed to generate a rich documentation.

Command-line Options
--------------------

* `-ragroot <dir>`
    Specify a root directory that all @declaredat paths will be displayed
    relative to in the generated HTML documents.

* `-linksource`
    Enables the linking of ReRAG and Java source files in the documentation.
    This will create HTML versions of all source files that are included in
    the documentation directory. Attributes will be linked to the HTML copy
    of it's ReRAG source file. If the @declaredat documentation tag is not
    present for some method or field, it will be linked to the HTML copy of
    it's Java source file.

* `-windowtitle <title>`
    Adds title the browser window for the generated documentation.

[1]: http://openjdk.java.net/legal/gplv2+ce.html
